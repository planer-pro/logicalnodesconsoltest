﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyNetSensors.LogicalNodes;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            LogicalNodesEngine engine = new LogicalNodesEngine();
            engine.OnDebugEngineMessage += EngineMessage;
            engine.OnDebugNodeMessage += NodeMessage;

            engine.Start();

            //Random rand = new Random(DateTime.Now.Millisecond);
            //while (true)
            //{
            //    int rnd = rand.Next(-1000, 1000);
            //}

            var node1 = new LogicalNodeOperatGenerator();
            engine.AddNode(node1);

            var node2 = new LogicalNodeOperatSwitch5x();
            engine.AddNode(node2);

            //var node3 = new LogicalNodeOperatCompareEqual();
            //engine.AddNode(node3);

            var viewConsole = new LogicalNodeSystemConsole();
            engine.AddNode(viewConsole);

            engine.AddLink(node1.Outputs[0], node2.Inputs[5]);
            //engine.AddLink(node2.Outputs[0], node3.Inputs[0]);
            engine.AddLink(node2.Outputs[0], viewConsole.Inputs[0]);

            node2.Inputs[0].Value = "hello";
            node2.Inputs[2].Value = "my";
            node2.Inputs[1].Value = "world";

            Console.WriteLine("------");
            Console.ReadLine();
        }

        private static void EngineMessage(string message)
        {
            Console.WriteLine("Engine - " + message + " ");
        }

        private static void NodeMessage(string message)
        {
            Console.WriteLine("Node - " + message);
        }
    }
}
