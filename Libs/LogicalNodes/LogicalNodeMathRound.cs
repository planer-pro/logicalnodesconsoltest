﻿//planer-pro copyright 2015 GPL - license.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNetSensors.LogicalNodes
{
   
    public class LogicalNodeMathRound : LogicalNode
    {
        /// <summary>
        /// Math Round (1 inputs, 1 output).
        /// </summary>
        public LogicalNodeMathRound() : base(1, 1)
        {
            this.Title = "Math Round";
            this.Type = "Math/Round";
        }

        public override void Loop()
        {
        }

        public override void OnInputChange(Input input)
        {


            try
            {
                Double a = Double.Parse(Inputs[0].Value);
                int b = (int)Math.Round(a);

                Debug($"Math/Round: [{a}] rounded to [{b}]");
                Outputs[0].Value = b.ToString();
            }
            catch
            {
                Debug($"Math/Round: [{Inputs[0].Value}] rounded to [NULL]");
                Outputs[0].Value = null;
            }
        }
    }
}