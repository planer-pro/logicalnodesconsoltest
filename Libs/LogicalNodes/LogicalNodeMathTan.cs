﻿/*  MyNetSensors 
    Copyright (C) 2015 Derwish <derwish.pro@gmail.com>
    License: http://www.gnu.org/licenses/gpl-3.0.txt  
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNetSensors.LogicalNodes
{
    public class LogicalNodeMathTan : LogicalNode
    {
        /// <summary>
        /// Math Tan (1 inputs, 1 output).
        /// </summary>
        public LogicalNodeMathTan() : base(1, 1)
        {
            this.Title = "Math Tan";
            this.Type = "Math/Tan";
        }

        public override void Loop()
        {
            //  Console.WriteLine( $"MATH LOOP {DateTime.Now} {Inputs[0].Value} {Inputs[1].Value}  {Outputs[0].Value}");
        }

        public override void OnInputChange(Input input)
        {
            try
            {
                Double a = Double.Parse(Inputs[0].Value);
                Double b = Math.Tan(a);

                Debug($"Math/Tan: Tan [{a}] = [{b}]");
                Outputs[0].Value = b.ToString();
            }
            catch
            {
                Debug($"Math/Tan: Tan [{Inputs[0].Value}] = [NULL]");
                Outputs[0].Value = null;
            }
        }
    }
}
