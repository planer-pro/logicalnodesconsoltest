﻿//planer-pro copyright 2015 GPL - license.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNetSensors.LogicalNodes
{

    public class LogicalNodeOperatCompareNotEqual : LogicalNode
    {
        /// <summary>
        /// Compare NotEqual (2 inputs, 1 output).
        /// </summary>
        public LogicalNodeOperatCompareNotEqual() : base(2, 1)
        {
            this.Title = "Compare NotEqual";
            this.Type = "Operation/Compare NotEqual";
        }

        public override void Loop()
        {
        }

        public override void OnInputChange(Input input)
        {

            try
            {
                Double a = Double.Parse(Inputs[0].Value);
                Double b = Double.Parse(Inputs[1].Value);

                if (a != b)
                {
                    Debug($"Operation/Compare NotEqual: [{a}] != [{b}]");
                    Outputs[0].Value = "1";
                }
                else
                {
                    Debug($"Operation/Compare NotEqual: [{a}] = [{b}]");
                    Outputs[0].Value = "0";
                }
            }
            catch
            {
                if (Inputs[0].Value != Inputs[1].Value)
                {
                    Debug($"Operation/Compare NotEqual: [{Inputs[0].Value}] != [{Inputs[1].Value}]");
                    Outputs[0].Value = "1";
                }
                else
                {
                    Debug($"Operation/Compare NotEqual: [{Inputs[0].Value}] = [{Inputs[1].Value}]");
                    Outputs[0].Value = "0";
                }
            }
        }
    }
}