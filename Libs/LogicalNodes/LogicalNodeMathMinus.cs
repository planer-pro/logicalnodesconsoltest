﻿//planer-pro copyright 2015 GPL - license.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNetSensors.LogicalNodes
{
   
    public class LogicalNodeMathMinus: LogicalNode
    {
        /// <summary>
        /// Math Minus (2 inputs, 1 output).
        /// </summary>
        public LogicalNodeMathMinus() : base(2, 1)
        {
            this.Title = "Math Minus";
            this.Type = "Math/Minus";
        }

        public override void Loop()
        {
        }

        public override void OnInputChange(Input input)
        {

            try
            {
                Double a = Double.Parse(Inputs[0].Value);
                Double b = Double.Parse(Inputs[1].Value);
                Double c = a - b;

                Debug($"Math/Minus: [{a}] - [{b}]  = [{c}]");
                Outputs[0].Value = c.ToString();
            }
            catch
            {
                Debug($"Math/Minus: [{Inputs[0].Value}] - [{Inputs[1].Value}]  = NULL");
                Outputs[0].Value = null;
            }
        }
    }
}