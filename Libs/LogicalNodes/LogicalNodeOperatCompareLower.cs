﻿//planer-pro copyright 2015 GPL - license.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNetSensors.LogicalNodes
{

    public class LogicalNodeOperatCompareLower : LogicalNode
    {
        /// <summary>
        /// Compare Lower (2 inputs, 1 output).
        /// </summary>
        public LogicalNodeOperatCompareLower() : base(2, 1)
        {
            this.Title = "Compare Lower";
            this.Type = "Operation/Compare Lower";
        }

        public override void Loop()
        {
        }

        public override void OnInputChange(Input input)
        {

            try
            {
                Double a = Double.Parse(Inputs[0].Value);
                Double b = Double.Parse(Inputs[1].Value);

                if (a < b)
                {
                    Debug($"Operation/Compare Lower: [{a}] < [{b}]");
                    Outputs[0].Value = "1";
                }
                else
                {
                    Debug($"Operation/Compare Lower: [{a}] > [{b}]");
                    Outputs[0].Value = "0";
                }
            }
            catch
            {
                Debug($"Operation/Compare Lower: input value is incorrect");
                Outputs[0].Value = null;
            }
        }
    }
}