﻿/*  MyNetSensors 
    Copyright (C) 2015 Derwish <derwish.pro@gmail.com>
    License: http://www.gnu.org/licenses/gpl-3.0.txt  
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNetSensors.LogicalNodes
{
    public class LogicalNodeLogicAnd : LogicalNode
    {
        /// <summary>
        /// Not (2 input, 1 output).
        /// </summary>
        public LogicalNodeLogicAnd() : base(2, 1)
        {
            this.Title = "Logic AND";
            this.Type = "Logic/AND";
        }

        public override void Loop()
        {
        }

        public override void OnInputChange(Input input)
        {
            if (Inputs[0].Value != "0" && Inputs[0].Value != "1")
            {
                Debug($"Logic/AND: [{Inputs[0].Value}] = [NULL]");
                Outputs[0].Value = null;

                return;
            }
            string result = "0";

            if (Inputs[0].Value == "1" && Inputs[1].Value == "1")
                result = "1";

            Debug($"Logic/AND: [{Inputs[0].Value}] AND [{Inputs[1].Value}] = [{result}]");

            Outputs[0].Value = result;
        }
    }
}
