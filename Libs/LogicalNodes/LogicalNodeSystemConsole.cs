﻿/*  MyNetSensors 
    Copyright (C) 2015 Derwish <derwish.pro@gmail.com>
    License: http://www.gnu.org/licenses/gpl-3.0.txt  
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNetSensors.LogicalNodes
{
    public class LogicalNodeSystemConsole:LogicalNode
    {
        /// <summary>
        /// Console (1 input).
        /// </summary>
        public LogicalNodeSystemConsole() : base(1, 0)
        {
            this.Title = "System Console";
            this.Type = "System/Console";
        }

        public override void Loop()
        {
        }

        public override void OnInputChange(Input input)
        {
            //Debug($"Console: {input.Value}");
            if(input.Value != null) Console.WriteLine($"LOGICAL NODE CONSOLE: [{input.Value}]");
            else Console.WriteLine($"System/Console: [NULL]");
        }
    }
}
