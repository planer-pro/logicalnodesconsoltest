﻿//planer-pro copyright 2015 GPL - license.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNetSensors.LogicalNodes
{
   
    public class LogicalNodeMathMultiply : LogicalNode
    {
        /// <summary>
        /// Math Multiply (2 inputs, 1 output).
        /// </summary>
        public LogicalNodeMathMultiply() : base(2, 1)
        {
            this.Title = "Math Multiply";
            this.Type = "Math/Multiply";
        }

        public override void Loop()
        {
        }

        public override void OnInputChange(Input input)
        {


            try
            {
                Double a = Double.Parse(Inputs[0].Value);
                Double b = Double.Parse(Inputs[1].Value);
                Double c = a * b;

                Debug($"Math/Multiply: [{a}] * [{b}]  = [{c}]");
                Outputs[0].Value = c.ToString();
            }
            catch
            {
                Debug($"Math/Multiply: [{Inputs[0].Value}] * [{Inputs[1].Value}]  = NULL");
                Outputs[0].Value = null;
            }
        }
    }
}