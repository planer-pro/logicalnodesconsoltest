﻿using MyNetSensors.LogicalNodes;

namespace MyNetSensors.LogicalNodesMySensors
{
    public class HardwareInput : Input
    {
        public int nodeId;
        public int sensorId;
    }
}
