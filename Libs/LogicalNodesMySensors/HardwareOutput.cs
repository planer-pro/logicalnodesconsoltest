﻿using MyNetSensors.LogicalNodes;

namespace MyNetSensors.LogicalNodesMySensors
{
    public class HardwareOutput:Output
    {
        public int nodeId;
        public int sensorId;
    }
}
